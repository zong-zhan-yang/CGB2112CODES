package com.blog.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

public class Product {
    @ApiModelProperty(value = "id", example = "1")
    private Integer id;
    @ApiModelProperty(value = "title", example = "Title-A")
    private String title;
    @ApiModelProperty(value = "url", example = "d:/aaa.jpg")
    private String url;
    @ApiModelProperty(value = "price", example = "100")
    private Double price;
    @ApiModelProperty(value = "oldPrice", example = "200")
    private Double oldPrice;
    @ApiModelProperty(value = "viewCount", example = "1")
    private Integer viewCount; //浏览量
    @ApiModelProperty(value = "saleCount", example = "1")
    private Integer saleCount;

    @ApiModelProperty(value = "created", example = "2022/05/20 10:12:12")
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm:ss",timezone = "GMT+8")
    private Date created;  //发布时间  导包java.util
    @ApiModelProperty(value = "categoryId", example = "1")
    private Integer categoryId; //商品分类id

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(Double oldPrice) {
        this.oldPrice = oldPrice;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public Integer getSaleCount() {
        return saleCount;
    }

    public void setSaleCount(Integer saleCount) {
        this.saleCount = saleCount;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", price=" + price +
                ", oldPrice=" + oldPrice +
                ", viewCount=" + viewCount +
                ", saleCount=" + saleCount +
                ", created=" + created +
                ", categoryId=" + categoryId +
                '}';
    }
}
