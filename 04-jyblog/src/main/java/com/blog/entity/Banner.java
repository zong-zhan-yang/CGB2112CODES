package com.blog.entity;


import io.swagger.annotations.ApiModelProperty;

public class Banner {
    //@ApiModelProperty(value = "id", example = "1")
    private Integer id;
    @ApiModelProperty(value = "图片地址", example = "/bc977279-e2f5-4a8f-ac59-36ce41848c0e.jpg")
    private String url;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Banner{" +
                "id=" + id +
                ", url='" + url + '\'' +
                '}';
    }
}
