package com.blog.controller;

import com.blog.dao.ProductMapper;
import com.blog.entity.Product;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.Date;
import java.util.List;

@Api(tags="商品管理")
@RestController
public class ProductController {

    @Autowired
    private ProductMapper mapper;

    @ApiOperationSupport(order = 10)
    @ApiOperation("前端商品查询")
    @GetMapping("/product/list/index")
    public List<Product> selectIndex(){
        return mapper.selectIndex();
    }

    @ApiOperationSupport(order = 20)
    @ApiOperation("后端商品查询")
    @GetMapping("/product/list/admin")
    public List<Product> selectAdmin(){
        return mapper.selectAdmin();
    }

    @ApiOperationSupport(order = 30)
    @ApiOperation("添加商品")
    @PostMapping("/product/insert")
    public void insert(@RequestBody Product product){
        System.out.println("product = " + product);
        product.setCreated(new Date());
        mapper.insert(product);
    }

    @ApiImplicitParam(name = "id", value = "商品id", example = "1",
            required = true, dataType = "long")
    @ApiOperationSupport(order = 40)
    @ApiOperation("删除商品")
    @DeleteMapping("/product/delete/{id}")
    public void delete(@PathVariable("id") int id){
        //得到文件路径并删除商品文件
        String url = mapper.selectUrlById(id);
        String filePath = "D:/file"+url;
        new File(filePath).delete();
        mapper.deleteById(id);
    }


    @ApiImplicitParam(name = "id", value = "商品id", example = "1",
            required = true, dataType = "long")
    @ApiOperationSupport(order = 50)
    @ApiOperation("基于id查询商品")
    @GetMapping("/product/selectById/{id}")
    public Product selectById(@PathVariable("id") int id, HttpSession session){
        System.out.println("id = " + id);
        //取出Session里面第一次浏览时保存的信息 如果没取到说明是第一次
        String info = (String) session.getAttribute("view"+id);
        if (info==null){
            //让浏览量+1
            mapper.updateViewCount(id);
            //此时是第一次访问 把商品id保存到Session里面
            session.setAttribute("view"+id,"浏览过");
        }
        return mapper.selectById(id);
    }
    @ApiImplicitParam(name = "id", value = "商品分类id", example = "1",
            required = true, dataType = "long")
    @ApiOperationSupport(order = 60)
    @ApiOperation("基于分类id查询商品")
    @GetMapping("/product/selectByCid/{cid}")
    public List<Product> selectByCid(@PathVariable("cid") int cid){
        System.out.println("cid = " + cid);
        return mapper.selectByCid(cid);
    }

    @ApiOperationSupport(order = 70)
    @ApiOperation("查询热卖商品")
    @GetMapping("/product/list/top")
    public List<Product> selectTop(){
        List<Product> list = mapper.selectTop();
        for (Product p:list) {
            if (p.getTitle().length()>5){//  非常漂...
                String title = p.getTitle().substring(0,3)+"...";
                p.setTitle(title);
            }
        }
        return list;
    }

    @ApiImplicitParam(name = "title", value = "商品标题", example = "商品A",
            required = true, dataType = "string")
    @ApiOperationSupport(order = 80)
    @ApiOperation("基于标题查询商品")
    @GetMapping("/product/selectByWd/{wd}")
    public List<Product> selectByWd(@PathVariable("wd") String wd){
        System.out.println("wd = " + wd);
        return mapper.selectByWd(wd);
    }

}

