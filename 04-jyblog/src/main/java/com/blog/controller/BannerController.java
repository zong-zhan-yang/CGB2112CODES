package com.blog.controller;

import com.blog.dao.BannerMapper;
import com.blog.entity.Banner;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.List;
@Api(tags = "广告条")
@RestController
public class BannerController {
    @Autowired
    private BannerMapper mapper;

    @ApiOperationSupport(order = 10)
    @ApiOperation("查询广告条")
    @GetMapping("/banner/list")
    public List<Banner> list(){
        return mapper.list();
    }

    @ApiOperation("添加广告条")
    @ApiOperationSupport(order=20)
    @PostMapping("/banner/insert")
    public void insert(@RequestBody Banner banner){
        System.out.println("banner = " + banner);
        mapper.insert(banner);
    }

    @ApiImplicitParam(name = "id", value = "广告条id", example = "1",
            required = true, dataType = "long")
    @ApiOperationSupport(order = 30)
    @ApiOperation("删除广告条")
    @DeleteMapping("/banner/delete/{id}")
    public void delete(@PathVariable("id") int id){
        //得到删除轮播图的图片名     /xxxx.jpg
        String url = mapper.selectUrlById(id);
        //得到文件的完整路径
        String filePath = "D:/file"+url;
        //删除文件
        new File(filePath).delete();

        mapper.deleteById(id);
    }
}

