package com.blog.controller;

import com.blog.dao.UserMapper;
import com.blog.entity.User;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Api(tags="用户管理")
@RestController
public class UserController {

    @Autowired
    private UserMapper mapper;

    @ApiOperationSupport(order = 10)
    @ApiOperation("登陆")
    @PostMapping("/login")
    public int login(@RequestBody User user, HttpSession session){
        User u = mapper.selectByUsername(user.getUsername());
        if (u!=null){
            if (u.getPassword().equals(user.getPassword())){
                session.setAttribute("u",u);
                return 1;//登录成功
            }
            return 3;//密码错误
        }
        return 2;//用户名不存在
    }

    @ApiOperationSupport(order = 20)
    @ApiOperation("查看登陆用户")
    @GetMapping("/currentUser")
    public User currentUser(HttpSession session){
        return (User) session.getAttribute("u");
    }

    @ApiOperation("登出")
    @GetMapping("/logout")
    public void logout(HttpSession session){
        session.removeAttribute("u");
    }

}
