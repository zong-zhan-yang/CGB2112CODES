package com.blog.controller;

import com.blog.dao.CategoryMapper;
import com.blog.entity.Category;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags="分类管理")
@RestController
public class CategoryController {
    @Autowired
    private CategoryMapper mapper;

    @ApiOperationSupport(order = 10)
    @ApiOperation("查询所有分类")
    @GetMapping("/category/list")
    public List<Category> list(){
        return mapper.list();
    }

    @ApiOperationSupport(order = 20)
    @ApiOperation("添加分类")
    @PostMapping("/category/insert")
    public void insert(@RequestBody Category category){
        mapper.insert(category);
    }

    @ApiImplicitParam(name = "id", value = "分类id", example = "1",
            required = true, dataType = "long")
    @ApiOperationSupport(order = 30)
    @ApiOperation("删除分类")
    @DeleteMapping("/category/delete/{id}")
    public void delete(@PathVariable int id){
        mapper.deleteById(id);
    }


}
