drop database if exists db_jingyun_secbuy;
create database db_jingyun_secbuy default character set utf8mb4;
use db_jingyun_secbuy;

create table `tb_secbuy_subject`
(
    `id` bigint primary key auto_increment ,
    `title` varchar(100) not null comment '活动标题',
    `start_time` date not null comment '开始日期',
    `end_time` date not null comment '结束日期',
    `remark` varchar(2000) comment '备注',
    `status` tinyint comment '状态 0未开始，1进行中,2已结束',
    `created_time` datetime not null comment '创建时间',
    `modified_time` datetime not null comment '修改时间'
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='秒杀主题';

create table `tb_secbuy_session`
(
    `id` bigint primary key auto_increment ,
    `name` varchar(100) not null comment '场次名称',
    `subject_id`  bigint  null comment '主题id',
    `start_time` time not null comment '每日开始时间',
    `end_time` time not null comment '每日结束时间',
    `remark` varchar(2000) comment '备注',
    `status` tinyint comment '状态 0未开始，1进行中,2已结束',
    `created_time` datetime not null comment '创建时间',
    `modified_time` datetime not null comment '修改时间'
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='秒杀场次';

create table `tb_secbuy_goods`
(
    `id`           bigint         auto_increment primary key,
    `sku_id`       bigint         not null comment '商品id',
    `subject_id`   bigint         null comment '活动id',
    `session_id`   bigint         null comment '场次id',
    `title`        varchar(100)   not null comment '标题',
    `picture`      varchar(150)   not null comment '推荐图片',
    `price`        decimal(10, 2) not null comment '原价格',
    `secbuy_price`   decimal(10, 2) not null comment '秒杀价格',
    `secbuy_stock`  int unsigned   not null comment '秒杀库存数',
    `remark` varchar(2000)  null comment '描述',
    `created_time` datetime not null comment '创建时间',
    `modified_time` datetime not null comment '修改时间'
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='秒杀商品';

create table `tb_secbuy_order`
(
    `id`               bigint         not null comment '主键'  primary key,
    `money`            decimal(10, 2) not null comment '支付金额',
    `member_id`        varchar(50)    not null comment '用户',
    `seller_id`        varchar(50)    not null comment '商家',
    `pay_time`         datetime       not null comment '支付时间',
    `status`           char           not null comment '状态，0未支付，1已支付',
    `receiver_address` varchar(200)   not null comment '收货人地址',
    `receiver_phone`   varchar(20)    not null comment '收货人电话',
    `receiver`         varchar(20)    not null comment '收货人',
    `transaction_id`   varchar(30)    not null comment '交易流水',
    `created_time`      datetime      not null comment '创建时间'
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='订单表';

create table `tb_secbuy_order_item`
(
    `id`               bigint         not null comment '主键'  primary key,
    `order_id`         bigint         not null COMMENT '订单id',
    `sku_id`           bigint         not null comment '秒杀商品ID(sku)',
    `title`            varchar(255)   not null COMMENT '商品SKU标题（冗余，历史）',
    `bar_code`         varchar(255)   not null COMMENT '商品SKU商品条型码（冗余）',
    `data`             varchar(2500)  DEFAULT NULL COMMENT '商品SKU全部属性，使用JSON格式表示（冗余）',
    `main_picture`     varchar(255)   DEFAULT NULL COMMENT '商品SKU图片URL（第1张）（冗余）',
    `price`            decimal(10,2)  DEFAULT NULL COMMENT '商品SKU单价（冗余，历史）',
    `quantity`         smallint unsigned DEFAULT NULL COMMENT '商品SKU购买数量',
    `created_time`      datetime      not null comment '创建时间'
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='订单表';

drop database if exists db_jingyun_member;
create database db_jingyun_member default character set utf8mb4;
use db_jingyun_member;
CREATE TABLE `tb_member`
(
    `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `username` varchar(64) NOT NULL COMMENT '用户名',
    `password` varchar(64) NOT NULL COMMENT '密码',
    `nickname` varchar(64) NOT NULL COMMENT '昵称',
    `icon` varchar(500) DEFAULT NULL COMMENT '头像',
    `level_id` int DEFAULT 0  COMMENT '等级',
    `phone` varchar(64) DEFAULT NULL COMMENT '手机号码',
    `email` varchar(50) default null unique comment '电子邮箱',
    `status` int(1) DEFAULT NULL COMMENT '帐号启用状态:0->禁用；1->启用',
    `gender` int(1) DEFAULT NULL COMMENT '性别：0->未知；1->男；2->女',
    `birthday` date DEFAULT NULL COMMENT '出生日期',
    `city` varchar(64) DEFAULT NULL COMMENT '所在城市',
    `job` varchar(100) DEFAULT NULL COMMENT '职业',
    `signature` varchar(200) DEFAULT NULL COMMENT '个性签名',
    `source_type` int(1) DEFAULT NULL COMMENT '用户来源',
    `integration` int(11) DEFAULT NULL COMMENT '积分',
    `growth` int(11) DEFAULT NULL COMMENT '成长值',
    `created_time` datetime DEFAULT NULL COMMENT '注册时间',
    `modified_time` datetime DEFAULT NULL COMMENT '修改时间',
    `login_count` int unsigned default 0 comment '累计登录次数（冗余）',
    `last_login_ip` varchar(50) default null comment '最后登录IP地址（冗余）',
    `last_login_time` datetime default null comment '最后登录时间（冗余）',
    PRIMARY KEY (`id`),
    UNIQUE KEY `idx_username` (`username`),
    UNIQUE KEY `idx_phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='会员表';

CREATE TABLE `tb_member_login_log`
(
    `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `member_id` bigint(20) DEFAULT NULL,
    `ip` varchar(64) DEFAULT NULL,
    `city` varchar(64) DEFAULT NULL,
    `login_type` int(1) DEFAULT NULL COMMENT '登录类型：0->PC；1->android;2->ios;3->小程序',
    `province` varchar(64) DEFAULT NULL,
    `created_time` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='会员登陆日志';

