package com.mall.secbuy.dao;

import com.mall.secbuy.pojo.entity.SecbuyGoods;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@SpringBootTest
public class GoodsMapperTests {
    @Autowired
    private SecbuyGoodsMapper secbuyGoodsMapper;
    @Test
    void testInsert(){
        SecbuyGoods goods=new SecbuyGoods();
        goods.setSubjectId(1L);
        goods.setSessionId(1L);
        goods.setSkuId(102L);
        goods.setTitle("茵曼马甲连衣裙两件套春季新款娃娃领色织格长袖背心裙套装");
        goods.setPicture("/5080711d-52bb-4e57-a645-1674857fd1f2.jpg");
        goods.setPrice(BigDecimal.valueOf(120));
        goods.setSecbuyPrice(BigDecimal.valueOf(10L));
        goods.setSecbuyStock(10);
        goods.setRemark("...");
        goods.setCreatedTime(LocalDateTime.now());
        goods.setModifiedTime(LocalDateTime.now());
        secbuyGoodsMapper.insert(goods);

    }
}
