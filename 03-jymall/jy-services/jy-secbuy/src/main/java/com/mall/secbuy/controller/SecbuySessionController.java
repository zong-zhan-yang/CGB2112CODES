package com.mall.secbuy.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.mall.common.domain.AjaxResult;
import com.mall.common.utils.PageUtils;
import com.mall.secbuy.pojo.dto.SecbuySessionDTO;
import com.mall.secbuy.pojo.entity.SecbuySession;
import com.mall.secbuy.service.SecbuySessionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "2. 场次管理")
@RestController
@RequestMapping("/secbuy/session")
public class SecbuySessionController {

    private SecbuySessionService secbuySessionService;
    public SecbuySessionController(SecbuySessionService secbuySessionService) {
        this.secbuySessionService = secbuySessionService;
    }

    @ApiOperationSupport(order = 20)
    @ApiOperation("查询所有场次信息")
    @GetMapping("/list")
    public AjaxResult doList(){
        return AjaxResult.success(PageUtils.startPage().doSelectPageInfo(()->{
            secbuySessionService.list();
        }));
    }

    @ApiOperationSupport(order = 20)
    @ApiOperation("查询所有场次信息")
    @PostMapping("/create")
    public AjaxResult doCreate(@RequestBody SecbuySessionDTO secbuySessionDTO){
        secbuySessionService.add(secbuySessionDTO);
        return AjaxResult.success("add ok");
    }


}
