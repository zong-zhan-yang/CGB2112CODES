package com.mall.secbuy.service;

import com.mall.secbuy.dao.SecbuySubjectMapper;
import com.mall.secbuy.pojo.dto.SecbuySubjectDTO;
import com.mall.secbuy.pojo.entity.SecbuySubject;
import com.mall.secbuy.pojo.vo.SecbuySubjectVO;
import com.mall.secbuy.service.SecbuySubjectService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class SecbuySubjectServiceImpl implements SecbuySubjectService {

    private final SecbuySubjectMapper secbuySubjectMapper;
    public SecbuySubjectServiceImpl(SecbuySubjectMapper secbuySubjectMapper){
        this.secbuySubjectMapper=secbuySubjectMapper;
    }

    @Override
    public List<SecbuySubjectVO> list() {
        return secbuySubjectMapper.list();
    }

    @Override
    public Long add(SecbuySubjectDTO secbuySubjectDTO) {
        SecbuySubject secbuySubject=new SecbuySubject();
        BeanUtils.copyProperties(secbuySubjectDTO,secbuySubject);
        secbuySubject.setCreatedTime(LocalDateTime.now());
        secbuySubject.setModifiedTime(LocalDateTime.now());
        System.out.println("service.add="+secbuySubject);
        return secbuySubjectMapper.insert(secbuySubject);
    }

}
