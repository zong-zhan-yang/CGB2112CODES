package com.mall.secbuy.dao;

import com.mall.secbuy.pojo.dto.SecbuySubjectDTO;
import com.mall.secbuy.pojo.entity.SecbuySubject;
import com.mall.secbuy.pojo.vo.SecbuySubjectVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SecbuySubjectMapper {

    List<SecbuySubjectVO> list();
    Long insert(SecbuySubject secbuySubject);
}
