package com.mall.secbuy.service;

import com.mall.secbuy.dao.SecbuySessionMapper;
import com.mall.secbuy.pojo.dto.SecbuySessionDTO;
import com.mall.secbuy.pojo.entity.SecbuySession;
import com.mall.secbuy.pojo.vo.SecbuySessionVO;
import com.mall.secbuy.service.SecbuySessionService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class SecbuySessionServiceImpl implements SecbuySessionService {
    private SecbuySessionMapper secbuySessionMapper;

    public SecbuySessionServiceImpl(SecbuySessionMapper secbuySessionMapper) {
        this.secbuySessionMapper = secbuySessionMapper;
    }

    @Override
    public List<SecbuySessionVO> list() {
        return secbuySessionMapper.list();
    }

    @Override
    public Long add(SecbuySessionDTO secbuySessionDTO) {
        SecbuySession secbuySession=new SecbuySession();
        BeanUtils.copyProperties(secbuySessionDTO,secbuySession);
        secbuySession.setCreatedTime(LocalDateTime.now());
        secbuySession.setModifiedTime(LocalDateTime.now());
        System.out.println(secbuySession);
        return secbuySessionMapper.insert(secbuySession);
    }
}
