package com.mall.secbuy.dao;

import com.mall.secbuy.pojo.entity.SecbuyGoods;
import com.mall.secbuy.pojo.vo.SecbuyGoodsVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SecbuyGoodsMapper {
    List<SecbuyGoodsVO> list();
    int insert(SecbuyGoods secbuyGoods);
}
