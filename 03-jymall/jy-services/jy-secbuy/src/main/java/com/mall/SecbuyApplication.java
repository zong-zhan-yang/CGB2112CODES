package com.mall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SecbuyApplication {
    public static void main(String[] args) {
        //System.out.println(LocalDateTime.now());
        SpringApplication.run(SecbuyApplication.class, args);
    }
}
