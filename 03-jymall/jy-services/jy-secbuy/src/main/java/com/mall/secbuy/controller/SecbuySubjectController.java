package com.mall.secbuy.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.mall.common.domain.AjaxResult;
import com.mall.common.utils.PageUtils;
import com.mall.secbuy.pojo.dto.SecbuySubjectDTO;
import com.mall.secbuy.pojo.vo.SecbuySubjectVO;
import com.mall.secbuy.service.SecbuySubjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Api(tags = "1. 主题管理")
@RestController
@RequestMapping("/secbuy/subject")
public class SecbuySubjectController {
    private SecbuySubjectService secbuySubjectService;
    public SecbuySubjectController(SecbuySubjectService secbuySubjectService){
        this.secbuySubjectService=secbuySubjectService;
    }

    @ApiOperationSupport(order = 20)
    @ApiOperation("查询所有主题信息")
    @GetMapping("/list")
    public AjaxResult doList(){
        List<SecbuySubjectVO> data = secbuySubjectService.list();
        return AjaxResult.success(PageUtils.startPage().doSelectPageInfo(()->{
            secbuySubjectService.list();
        }));
    }

    @ApiOperationSupport(order = 30)
    @ApiOperation("创建新主题")
    @PostMapping("/create")
    public AjaxResult doCreate(@RequestBody SecbuySubjectDTO secbuySubjectDTO){
        System.out.println(secbuySubjectDTO);
         secbuySubjectService.add(secbuySubjectDTO);
         return AjaxResult.success("create ok");
    }

}
