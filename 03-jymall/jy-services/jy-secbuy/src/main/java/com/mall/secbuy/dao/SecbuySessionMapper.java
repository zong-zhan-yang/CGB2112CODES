package com.mall.secbuy.dao;

import com.mall.secbuy.pojo.entity.SecbuySession;
import com.mall.secbuy.pojo.entity.SecbuySubject;
import com.mall.secbuy.pojo.vo.SecbuySessionVO;
import com.mall.secbuy.pojo.vo.SecbuySubjectVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SecbuySessionMapper {
    List<SecbuySessionVO> list();
    Long insert(SecbuySession secbuySession);
}
