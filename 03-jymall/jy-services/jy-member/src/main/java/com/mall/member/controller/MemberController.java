package com.mall.member.controller;
import com.mall.common.domain.AjaxResult;
import com.mall.common.service.RedisService;
import com.mall.common.utils.AssertUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Random;

@RestController
@RequestMapping("/member")
public class MemberController {

    @ApiOperation("会员注册")
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult register(@RequestParam String username,
                               @RequestParam String password,
                               @RequestParam String telephone,
                               @RequestParam String authCode) {
        //memberService.register(username, password, telephone, authCode);
        return AjaxResult.success("注册OK");
    }


}
