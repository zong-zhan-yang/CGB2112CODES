package com.mall.member.service.impl;

import com.mall.common.service.RedisService;
import com.mall.common.utils.AssertUtils;
import com.mall.member.pojo.entity.Member;
import com.mall.member.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Random;

@Service
public class MemberServiceImpl implements MemberService {
    @Autowired
    private RedisService redisService;
    @Override
    public Member createMember(String username, String password, String telephone, String authCode) {
        //验证验证码
        if(!verifyAuthCode(authCode,telephone)){
            AssertUtils.fail("验证码错误");
        }
        return null;
    }
    @Override
    public Member retrieveMember(String username) {
        return null;
    }

    @Override
    public String generateAuthCode(String telephone) {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for(int i=0;i<6;i++){
            sb.append(random.nextInt(10));
        }
        redisService.set(telephone, sb.toString());
        return sb.toString();
    }
    //对输入的验证码进行校验
    private boolean verifyAuthCode(String authCode, String telephone){
        if(StringUtils.isEmpty(authCode)){
            return false;
        }
        String realAuthCode =(String)redisService.get(telephone);
        return authCode.equals(realAuthCode);
    }
}
