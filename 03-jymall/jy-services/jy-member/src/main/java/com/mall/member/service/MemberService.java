package com.mall.member.service;

import com.mall.member.pojo.entity.Member;

public interface MemberService {

    /**
     * 会员注册
     * @param username
     * @param password
     * @param telephone
     * @param authCode
     * @return
     */
    Member createMember(String username, String password, String telephone, String authCode);

    /**
     * 基于用户名查询会员
     * @param username
     * @return
     */
    Member retrieveMember(String username);

    /**
     * 生成验证码
     * @param telephone
     * @return
     */
    String generateAuthCode(String telephone);
}