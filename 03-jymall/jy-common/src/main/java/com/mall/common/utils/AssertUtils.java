package com.mall.common.utils;

import com.mall.common.exception.ApiException;

public class AssertUtils {
    public static void fail(String message) {
        throw new ApiException(message);
    }
}
