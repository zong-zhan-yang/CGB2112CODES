package com.mall.secbuy.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
public class SecbuySessionDTO {

    @ApiModelProperty(value="主题id",position = 2)
    private Long subjectId;
    @NotNull(message = "请填写场次名称！")
    @Pattern(regexp = ".{2,16}", message ="场次标题必须由2~16字符组成！")
    @ApiModelProperty(value="场次标题",position = 3,required = true)
    private String name;
    @ApiModelProperty(value = "开始时间",position = 4,required = true)
    private LocalTime startTime;
    @ApiModelProperty(value="结束时间",position = 5,required = true)
    private LocalTime endTime;
    @ApiModelProperty(value = "状态", position = 6,required = true)
    private int status;
    @ApiModelProperty(value = "备注", position = 7)
    private String remark;
}
