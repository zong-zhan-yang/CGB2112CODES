package com.mall.secbuy.pojo.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class SecbuyGoods {
    private Long id;
    private Long skuId;
    private Long subjectId;
    private Long sessionId;
    private String title;
    private String picture;
    private BigDecimal price;
    private BigDecimal secbuyPrice;
    private Integer secbuyStock;
    private String remark;
    private LocalDateTime createdTime;
    private LocalDateTime modifiedTime;
}
