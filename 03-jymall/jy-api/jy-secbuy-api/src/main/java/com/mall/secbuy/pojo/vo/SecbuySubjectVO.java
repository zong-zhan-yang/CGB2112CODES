package com.mall.secbuy.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 主题对象，用于封装响应到客户端的主题信息
 */
@Data
public class SecbuySubjectVO implements Serializable {
    private static final long serialVersionUID = 2772114833009929522L;
    @ApiModelProperty(value = "主题id", position = 1)
    private Long id;
    @ApiModelProperty(value = "主题标题", position = 2)
    private String title;
    @ApiModelProperty(value = "主题开始日期", position = 3)
    private LocalDate startTime;
    @ApiModelProperty(value = "主题结束日期", position = 4)
    private LocalDate endTime;
    @ApiModelProperty(value = "主题状态", position = 5)
    private int status;
    @ApiModelProperty(value = "主题创建时间", position = 6)
    private LocalDateTime createdTime;
}
