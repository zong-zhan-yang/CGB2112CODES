package com.mall.secbuy.service;

import com.mall.secbuy.pojo.dto.SecbuySessionDTO;
import com.mall.secbuy.pojo.dto.SecbuySubjectDTO;
import com.mall.secbuy.pojo.vo.SecbuySessionVO;
import com.mall.secbuy.pojo.vo.SecbuySubjectVO;

import java.util.List;

public interface SecbuySessionService {
    List<SecbuySessionVO> list();
    Long add(SecbuySessionDTO secbuySessionDTO);
}
