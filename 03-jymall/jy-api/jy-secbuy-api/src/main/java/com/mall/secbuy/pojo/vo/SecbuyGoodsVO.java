package com.mall.secbuy.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class SecbuyGoodsVO {

    @ApiModelProperty(value = "商品序号", position = 1)
    private Long id;
    @ApiModelProperty(value = "商品sku", position = 2)
    private Long skuId;
    @ApiModelProperty(value = "主题id", position = 3)
    private Long subjectId;
    @ApiModelProperty(value = "场次id", position = 4)
    private Long sessionId;
    @ApiModelProperty(value = "标题", position = 5)
    private String title;
    @ApiModelProperty(value = "商品推荐图片", position = 6)
    private String picture;
    @ApiModelProperty(value = "商品原价", position = 7)
    private BigDecimal price;
    @ApiModelProperty(value = "商品秒杀价", position = 8)
    private BigDecimal secbuyPrice;
    @ApiModelProperty(value = "商品秒杀库存", position = 9)
    private Integer secbuyStock;

}
