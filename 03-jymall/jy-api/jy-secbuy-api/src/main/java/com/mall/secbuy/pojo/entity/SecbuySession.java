package com.mall.secbuy.pojo.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * 秒杀场次
 */
@Data
public class SecbuySession implements Serializable {
    private static final long serialVersionUID = -3341621684289295422L;
    private Long id;
    private Long subjectId;
    private String name;
    private LocalTime startTime;
    private LocalTime endTime;
    private int status;
    private String remark;
    private LocalDateTime createdTime;
    private LocalDateTime modifiedTime;
}
