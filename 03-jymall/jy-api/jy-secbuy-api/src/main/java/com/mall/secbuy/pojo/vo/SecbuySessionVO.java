package com.mall.secbuy.pojo.vo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * 主题场次，用于封装响应到客户端的场次信息
 */
@Data
public class SecbuySessionVO implements Serializable {
    @ApiModelProperty(value = "场次id",position = 1)
    private Long id;

    @ApiModelProperty(value="场次名称",position = 2)
    private String name;

    @ApiModelProperty(value="主题id",position = 3)
    private Long subjectId;

    @ApiModelProperty(value="主题标题",position = 4)
    private String title;

    @ApiModelProperty(value = "开始时间",position = 5)
    private LocalTime startTime;

    @ApiModelProperty(value="结束时间",position = 6)
    private LocalTime endTime;

    @ApiModelProperty(value = "状态",position = 7)
    private int status;

    @ApiModelProperty(value="创建时间",position = 8)
    private LocalDateTime createdTime;
}
