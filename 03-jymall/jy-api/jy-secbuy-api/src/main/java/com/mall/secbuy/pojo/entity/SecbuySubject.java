package com.mall.secbuy.pojo.entity;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 秒杀主题
 */
@Data
public class SecbuySubject implements Serializable {
    private static final long serialVersionUID = -2530637450820557614L;
    private Long id;
    private String title;
    private LocalDate startTime;
    private LocalDate endTime;
    private int status;
    private String remark;
    private LocalDateTime createdTime;
    private LocalDateTime modifiedTime;
}


