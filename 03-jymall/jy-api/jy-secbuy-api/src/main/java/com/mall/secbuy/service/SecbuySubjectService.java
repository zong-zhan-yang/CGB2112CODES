package com.mall.secbuy.service;

import com.mall.secbuy.pojo.dto.SecbuySubjectDTO;
import com.mall.secbuy.pojo.vo.SecbuySubjectVO;

import java.util.List;

public interface SecbuySubjectService {
     List<SecbuySubjectVO> list();
     Long add(SecbuySubjectDTO secbuySubjectDTO);
}
