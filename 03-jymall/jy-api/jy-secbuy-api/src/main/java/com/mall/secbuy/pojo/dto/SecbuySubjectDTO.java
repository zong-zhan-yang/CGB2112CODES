package com.mall.secbuy.pojo.dto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class SecbuySubjectDTO implements Serializable {
    private static final long serialVersionUID = -8676640924546959888L;

    @ApiModelProperty(value = "主题标题", position = 1,required = true)
    @NotNull(message = "请填写主题名称！")
    @Pattern(regexp = ".{2,16}", message = "主题名称必须由2~16字符组成！")
    private String title;
    @ApiModelProperty(value = "主题开始时间", position = 2,required = true)
    private LocalDate startTime;
    @ApiModelProperty(value = "主题结束时间", position = 3,required = true)
    private LocalDate endTime;
    @ApiModelProperty(value = "状态", position = 4,required = true)
    private int status;
    @ApiModelProperty(value = "备注", position = 5)
    private String remark;
}
