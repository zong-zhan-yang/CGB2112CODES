package com.mall.member.pojo.entity;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Member implements Serializable {
    private static final long serialVersionUID = 8779002223896212649L;

    private Long id;
    /**用户id*/
    private String username;
    /**密码*/
    private String password;
    /**昵称*/
    private String nickname;
    /**头像*/
    private String icon;
    /**会员等级*/
    private int level;
    /**手机号*/
    private String phone;
    /**邮箱*/
    private String email;
    /**状态*/
    private int status=1;
    /**性别*/
    private int gender=0;
    /**出生日期*/
    private String birthday;
    /**城市*/
    private String city;
    /**职业*/
    private String job;
    /**个人签名*/
    private String signature;
    /**用户来源*/
    private String sourceType;
    /**用户积分*/
    private int integration;
    /**用户成长值*/
    private int growth;
    /**创建时间*/
    private Date createTime;
    /**更新时间*/
    private Date updateTime;
    /**登陆次数*/
    private int loginCount;
    /**登陆ip*/
    private String lastLoginIp;
    /**登陆时间*/
    private Date lastLoginTime;

}