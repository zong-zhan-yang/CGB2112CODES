package com.mall.member.pojo.entity;
import lombok.Data;

@Data
public class MemberLoginInfo {
    private Long id;
    /**会员id*/
    private Long memberId;
    /**登陆ip*/
    private String ip;
    /**登陆城市*/
    private String city;
    /**登陆省份*/
    private String province;
    /**登陆类型*/
    private String loginType;
    /**创建日期*/
    private String createTime;
}
