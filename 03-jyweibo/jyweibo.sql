DROP DATABASE IF EXISTS `jy-weibo`;
CREATE DATABASE  `jy-weibo` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
USE `jy-weibo`;

create table weibo
(id int primary key auto_increment,
content varchar(255),
urls varchar(255),
nick varchar(50),
created timestamp,
user_id int
) charset=utf8;

create table comment
(
id int primary key auto_increment,
content varchar(255),
nick varchar(50),
weibo_id int
)charset=utf8;

create table user
(
id int primary key auto_increment,
username varchar(50),
password varchar(50),
nick varchar(50)
)charset=utf8;
insert into user values(null,'jack','123456','jack');

