package com.spring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/provider")
public class ProviderController {
    private static final Logger log=
            LoggerFactory.getLogger(ProviderController.class);

    @Value("${server.port:8080}")
    private String server;

    @GetMapping("/echo/{data}")
    public String doEcho1(@PathVariable("data") String data){
        log.debug("before:", System.currentTimeMillis());
        String content= server +" say: hello "+data;
        log.debug("content:"+content);
        log.debug("after:", System.currentTimeMillis());
        return content;
    }
}
