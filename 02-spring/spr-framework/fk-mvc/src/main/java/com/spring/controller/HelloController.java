package com.spring.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 定义Controller对象，处理客户端的请求。
 * 思考：这个Controller对象如何去加载
 */
@RestController
public class HelloController {

    public HelloController(){
        System.out.println("HelloController()");
    }

    @GetMapping("/hello")
    public String doSayHello(){
        return "hello spring mvc";
    }

}
