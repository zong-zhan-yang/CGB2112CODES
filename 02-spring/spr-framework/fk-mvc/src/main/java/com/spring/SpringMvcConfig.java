package com.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableWebMvc //一定要开启springmvc
@ComponentScan("com.spring.controller")
public class SpringMvcConfig implements WebMvcConfigurer {

    //这里可以写一些拦截器相关等配置
}
