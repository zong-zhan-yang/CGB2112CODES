package com.spring;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Spring Webmvc启动时会自动加载此类(底层是一种SPI机制)
 */
public class WebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    /**
     * 加载service,dao等配置类
     * @return
     */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        System.out.println("==getRootConfigClasses()==");
        return new Class[]{SpringConfig.class};
    }

    /**
     * 加载spring mvc 配置类
     * @return
     */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        System.out.println("==getServletConfigClasses()==");
        return new Class[]{SpringMvcConfig.class};
    }

    /**
     * 加载Servlet映射信息
     * @return
     */
    @Override
    protected String[] getServletMappings() {
        System.out.println("==getServletMappings()==");
        return new String[]{"/*"};
    }
}
