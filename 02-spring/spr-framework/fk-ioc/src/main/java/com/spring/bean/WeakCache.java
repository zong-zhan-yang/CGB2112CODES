package com.spring.bean;

import org.springframework.stereotype.Component;
import javax.annotation.Resource;

@Component
public class WeakCache implements Cache{
    /**
     *  @Resource 描述属性时，用于告诉系统底层
     *  按名字为属性注入值。
     *
     */
    @Resource(name="simpleCache")
    private Cache cache;
}
