package com.spring.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:jdbc.properties")//读取配置文件中内容并将内容赋值给Environment对象
public class JdbcConfig {
    @Value("${spring.jdbc.url}")//这种写法，系统底层会读取Environment对象中的属性值
    private String url;
    @Value("${spring.jdbc.username}")
    private String username;
    @Value("${spring.jdbc.password}")
    private String password;

    @Override
    public String toString() {
        return "JdbcConfig{" +
                "url='" + url + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
