package com.spring;
import com.example.Container;
import com.spring.bean.SimpleCache;
import com.spring.config.BeanConfig;
import com.spring.config.JdbcConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;

/**
 * 告诉底层默认对此注解描述的类所在包以及子包中的类进行扫描，然后对类进行加载并检测
 * 类是否是spring的一个Bean组件(类上是否有@Component注解等描述)，假如是则将类的信息
 * 存储到Map<String,BeanDefinition>中，然后系统底层会基于这些类的配置通过反射技术创建
 * 其对象。并且可以基于类的作用域对类的实例进行存储Map<String,Object>
 */
@Import(Container.class) //加载其它包中的类,这种方式加载的类，其Bean的名字为类全名。
@ComponentScan
//@ComponentScan("com.spring")//这里是指定对哪个包中的资源进行扫描
public class IocApplication {
    public static void main(String[] args) {
        //1.使用SimpleCache对象
        //SimpleCache simpleCache=new SimpleCache();
        //2.基于Spring创建SimpleCache对象
        //2.1初始化spring容器
        AnnotationConfigApplicationContext context=
                new AnnotationConfigApplicationContext(IocApplication.class);
        //2.2从spring容器获取bean对象
        SimpleCache simpleCache1 =
                context.getBean("simpleCache", SimpleCache.class);
        System.out.println(simpleCache1);

        Container container =
                context.getBean("com.example.Container",Container.class);
        System.out.println(container);



        context.close();

    }
}
