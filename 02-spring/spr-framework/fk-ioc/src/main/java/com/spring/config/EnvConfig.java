package com.spring.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class EnvConfig {
    //Spring会从容器中基于类型查询Environment对象，假如类型只有一个则直接注入
    //假如类型有多个，还会基于属性名去查找，有相同名字的，则直接注入，没有则抛出异常。
    //@Autowired //直接描述属性
    private Environment environment;
    /**
     * 构造注入：通过@Autowired注解描述类中的构造方法
     * 说明：假如有一个构造方法，@Autowired注解可以省略
     * @param environment
     */
    @Autowired
    public EnvConfig(Environment environment){
        this.environment=environment;
    }

    public Environment getEnvironment() {
        return environment;
    }
}
