package com.spring.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class SoftCache implements Cache{
//    @Autowired
//    @Qualifier("simpleCache")
      private Cache cache;

      @Autowired
      public SoftCache(@Qualifier("simpleCache") Cache cache) {
        this.cache = cache;
      }
}
