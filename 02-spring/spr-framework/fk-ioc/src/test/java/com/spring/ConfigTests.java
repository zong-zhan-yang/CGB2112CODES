package com.spring;

import com.spring.config.BeanConfig;
import com.spring.config.EnvConfig;
import com.spring.config.JdbcConfig;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;

public class ConfigTests {
    private AnnotationConfigApplicationContext context;
    @Before
    public void init(){
        context= new AnnotationConfigApplicationContext(IocApplication.class);
    }
    @Test
    public void testJdbcConfig(){
        JdbcConfig jdbcConfig = context.getBean("jdbcConfig", JdbcConfig.class);
        System.out.println(jdbcConfig);
    }
    @Test
    public void testBeanConfig(){
        BeanConfig beanConfig = context.getBean("beanConfig", BeanConfig.class);
        System.out.println(beanConfig);
    }
    @Test
    public void testEvnConfig(){
        EnvConfig envConfig = context.getBean("envConfig", EnvConfig.class);
        Environment environment = envConfig.getEnvironment();
        System.out.println(environment);
        System.out.println(environment.getProperty("spring.jdbc.url"));
        System.out.println(environment.getProperty("spring.jdbc.username"));
        System.out.println(environment.getProperty("spring.jdbc.password"));
    }

}
