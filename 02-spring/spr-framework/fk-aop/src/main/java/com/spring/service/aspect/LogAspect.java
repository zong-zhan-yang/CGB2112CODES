package com.spring.service.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LogAspect{

    @Pointcut("@annotation(com.spring.annotation.RequiredLog)")
    public void doLog(){}

    @Around("doLog()")
    public Object doAround(ProceedingJoinPoint joinPoint)
            throws Throwable{
        long start=System.currentTimeMillis();
        System.out.println("start:"+start);
        Object object=joinPoint.proceed();
        System.out.println(object);
        long end=System.currentTimeMillis();
        System.out.println("end:"+end);
        return object;
    }
}
