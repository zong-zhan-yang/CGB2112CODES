package com.spring.service.impl;

import com.spring.annotation.RequiredLog;
import com.spring.service.ArticleService;
import org.springframework.stereotype.Service;

@Service
public class ArticleServiceImpl implements ArticleService {
    @RequiredLog
    @Override
    public Object selectById(Long id) {
        //long start=System.currentTimeMillis();
        //System.out.println("start:"+start);
        //假设这个结果来在数据库(这是核心业务)
        String content="The Article Content By "+id;
        //long end=System.currentTimeMillis();
        //System.out.println("end:"+end);
        return content;
    }
}
