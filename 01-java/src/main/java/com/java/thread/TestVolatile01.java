package com.java.thread;
class Looper{
	private volatile boolean isStop;
	public void loop() {
		for(;;) {
			if(isStop)break;
		}
	}
	public void stop() {
		isStop=true;
	}
}
public class TestVolatile01 {
	public static void main(String[] args) throws InterruptedException {
		//Thread main=Thread.currentThread();
		Looper looper=new Looper();
		Thread t=new Thread(){
			@Override
			public void run() {
				looper.loop();
			}
		};
		t.start();
		t.join(3000);
		
		looper.stop();
		
		
	}
	
}
