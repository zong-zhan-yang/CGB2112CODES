package com.java.jvm.bytecode;

class Rectangle{//矩形
    int x=10;
    public Rectangle(){
        doPrint();
        this.x=20;
    }
    public void doPrint(){
        System.out.println("Rectangle.x="+this.x);
    }
}
class Square extends Rectangle{//正方形
    int x=30;
    public Square(){
        doPrint();
        this.x=40;
    }
    @Override
    public void doPrint() {
        System.out.println("Square.x="+this.x);
    }
}

public class ExtendsTests {
    public static void main(String[] args) {
        Rectangle r=new Square();
        System.out.println(r.x);
    }
}
