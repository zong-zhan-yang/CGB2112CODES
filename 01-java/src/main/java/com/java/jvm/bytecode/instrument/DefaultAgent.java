package com.java.jvm.bytecode.instrument;

import com.java.jvm.bytecode.service.CycleService;

import java.lang.instrument.Instrumentation;

public class DefaultAgent {
    /**
     * agentmain 函数，在主程序运行之后执行。
     * @param args
     * @param inst，此类提供检测 Java 编程语言代码所需的服务。
     * 通过它，我们可以操作JVM，并修改Class中的一些内容。
     */
    public static void agentmain(String args, Instrumentation inst) {
        //注册一个转换类文件转换器。
        inst.addTransformer(new DefaultClassTransformer(), true);
        try {
            //重定义类并载入新的字节码
            inst.retransformClasses(CycleService.class);
            System.out.println("Agent Load Done.");
        } catch (Exception e) {
            System.out.println("agent load failed!");
        }
    }
}
