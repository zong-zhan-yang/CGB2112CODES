package com.java.jvm.bytecode;

public class StringCompareTests {
    public static void main(String[] args) {
        String s1="hello"+"world";
        String s2="helloworld";
        String s3="hello";
        String s4="world";
        String s5=s3+s4;
        System.out.println(s1==s2);
        System.out.println(s1==s5);
    }
}
