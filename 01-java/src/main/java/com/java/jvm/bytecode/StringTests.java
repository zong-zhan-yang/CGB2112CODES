package com.java.jvm.bytecode;

public class StringTests {
    static String doConcat1(){
        String str="";
        for(int i=0;i<10;i++){
            str=str+"java,";
        }
        return str;
    }
    static String doConcat2(){
        StringBuffer sb=new StringBuffer();
        for(int i=0;i<10;i++){
            sb.append("java,");
        }
        return sb.toString();
    }

    static String doReturn(){
        String str="java";
        try{
            return str;
        }finally {
            str="go";
        }
    }
    public static void main(String[] args) {
        System.out.println(doReturn());
    }
}
