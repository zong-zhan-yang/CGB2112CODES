package com.java.jvm.bytecode;

import com.java.jvm.bytecode.service.CycleService;

import java.lang.management.ManagementFactory;

public class CycleServiceTests {
    public static void main(String[] args) {
        String name = ManagementFactory.getRuntimeMXBean().getName();
        String s = name.split("@")[0];
        //打印当前Pid
        System.out.println("pid:"+s);
        CycleService cs=new CycleService();
        while(true) {
            try {
                cs.doCycle();
                Thread.sleep(3000);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
