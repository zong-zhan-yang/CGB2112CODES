package com.java.jvm;
public class ObjectScalarReplaceTests {
    public static void main(String args[]) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            alloc();
        }
        long end = System.currentTimeMillis();
        System.out.println("花费的时间为： " + (end - start) + " ms");
    }
    private static void alloc() {
        Point point = new Point(1,2);
        //System.out.println("point.x" + point.x + ";point.y" + point.y);
    }
    static class Point {
        private int x;
        private int y;
        public Point(int x,int y){
            this.x=x;
            this.y=y;
        }
    }
}
