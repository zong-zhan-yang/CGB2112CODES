package com.java.jvm.gc;

public class RefCountTests {

    private byte[] bigSize = new byte[10 * 1024 * 1024];//10MB

    private Object ref = null;

    public static void main(String[] args) {
        RefCountTests obj1 = new RefCountTests();
        RefCountTests obj2 = new RefCountTests();

        obj1.ref = obj2;
        obj2.ref = obj1;

        obj1 = null;
        obj2 = null;
        //显式的执行垃圾回收行为
        //这里发生GC，obj1和obj2能否被回收？
        System.gc();

    }
}
