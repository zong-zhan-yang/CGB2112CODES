package com.java.jvm.gc;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

class Point{
    void distance(){
        System.out.println("distance");
    }
    protected void finalize() throws Throwable {
        System.out.println(" finalize() ");
    }
}
//-Xmx5m -Xms5m -XX:+PrintGC
public class RefTest01 {
    public static void main(String[] args) throws InterruptedException {
        //1.强引用(有强引用引用的对象，生命力非常顽强，即便是内存溢出也不会被回收)
         //Point p1=new Point();//这里的p1就是强引用
         //p1.distance();

        //2.软引用(内存不足时可能会对软引用应用的对象进行回收)
         //SoftReference<Point> p2=new SoftReference<>(new Point());
         //p2.get().distance();

        //3.弱引用(一般在触发GC时，此引用引用的对象就可能被回收,生命力比软引用还要薄弱)
         //WeakReference<Point> p3=new WeakReference<>(new Point());
         //p3.get().distance();

        //4.虚引用(相当于没有引用，一般只记录对象被回收了)
        ReferenceQueue<Point> refQueue=new ReferenceQueue<>();
        PhantomReference rf=new PhantomReference(new Point(), refQueue);
        System.gc();

        List<byte[]> list=new ArrayList<>();
        for(int i=0;i<4;i++){
            list.add(new byte[1024*1024]);
        }
    }
}
