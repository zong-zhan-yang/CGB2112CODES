package com.java.jvm;
//javap -p -v MethodAreaStructureTests.class > class.txt
public class MethodAreaStructureTests {
    private int size;
    private static int count=10;
    public void doChangeSize(){
        size++;
    }
    public static void doChangeCount(){
        count++;
    }
    public static void main(String[] args) {
        System.out.println(count);
    }
}
