package com.java.jvm.leak;

/**
 * 在 Java 7 以后，在 heap 内存当中，
 * 专门新增了一个 string pool 专用的常量池 。
 * 我们应该把常量字符串往这个池里面放，用于提高对象重复使用的效率。
 */
public class StringInternTests {
    public static void main(String[] args) {
        String s1 = new String("a"); // 在 heap 内存创建对象，并返回
        String s2 = "a"; // 在 常量池 存创建对象，并返回
        String s3 = "a"; // 在 常量池 存创建对象，并返回， 此处的 s3对象其实是 s2那个对象。

        System.out.println("s1==s2?" + (s1 == s2));//false
        System.out.println("s2==s3?" + (s2 == s3));//false

        s1 = s1.intern(); // 去常量池寻找 ’a‘ 是否存在？ 并且返回对引用。 此处返回的值是 s2
        System.out.println("s1==s2?" + (s1 == s2));
    }
}
