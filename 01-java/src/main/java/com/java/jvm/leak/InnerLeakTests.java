package com.java.jvm.leak;

class OuterClass {
    private int o;
    private byte[] bigObject = new byte[1024 * 10]; // 为了效果明显，做这样的测试。

    class InnerClass {
        private int i;

        int add() {
            return i++;
        }
    }

    static class StaticNestedClass {
        private int n;

        int sub() {
            return n--;
        }
    }
}
public class InnerLeakTests {
    public static void main(String[] args) {
//        for (int i = 0; i < 1024 * 10000; i++) {
//            OuterClass o = new OuterClass();
//            OuterClass.InnerClass innerClass = o.new InnerClass();
//            innerClass.add();
//        }
    }
}
