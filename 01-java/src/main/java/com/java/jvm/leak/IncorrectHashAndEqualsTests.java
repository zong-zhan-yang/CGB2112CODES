package com.java.jvm.leak;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

class Pig {
    private String name;
    public Pig(String name) {
        this.name = name;
    }


}
public class IncorrectHashAndEqualsTests {
    public static void main(String[] args) throws Exception{
        TimeUnit.SECONDS.sleep(10);
        Map<Pig, Integer> pigs = new HashMap<>();
        for (int i = 0; i < 10000 * 1000; i++) {
            pigs.put(new Pig("佩奇"), i);
            //TimeUnit.MILLISECONDS.sleep(2);
        }
        System.out.println(pigs.size());
    }
}
