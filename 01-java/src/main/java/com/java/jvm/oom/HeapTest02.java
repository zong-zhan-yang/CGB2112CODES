package com.java.jvm.oom;
//-XX:+PrintGC
//最大堆-Xmx5m 最小堆-Xms5m
public class HeapTest02 {
    static byte[] b2;
    public static void main(String[] args) {
        while(true) {
            doMethod();
        }
    }
    static void doMethod(){
            //try{Thread.sleep(500);}catch (Exception e){}
            //byte[] b1 = new byte[1];//小对象未逃逸，可以直接分配在栈上，好处是不需要GC
            b2 = new byte[1];//小对象已逃逸(有外部引用)，只能分配在堆上
    }
}
