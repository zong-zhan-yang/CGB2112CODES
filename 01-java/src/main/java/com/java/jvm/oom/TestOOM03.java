package com.java.jvm.oom;
import java.nio.ByteBuffer;

public class TestOOM03 {

	public static void main(String[] args) {
		for(int i=0;i<Integer.MAX_VALUE;i++) {
		  ByteBuffer.allocateDirect(1024*1024*1024*1024);
		}
	}
}
