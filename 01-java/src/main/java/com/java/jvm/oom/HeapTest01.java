package com.java.jvm.oom;

//-XX:+PrintGC
//-XX:+PrintGCDetails

/**
 * 编写如下代码，基于如下两种情况运行程序,检测输出结果：
 * 1.直接运行程序
 * 2.设置如下两个JVM参数后运行程序
 * -Xmx5m -Xms5m
 */
public class HeapTest01 {
    public static void main(String[] args) {
       byte []data1=new byte[1024*1024];
       byte []data2=new byte[1024*1024];
       byte []data3=new byte[1024*1024];
       byte []data4=new byte[1024*1024];
       byte []data5=new byte[1024*1024];
    }
}
