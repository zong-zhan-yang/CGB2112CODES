package com.java.jvm.oom;

import java.util.ArrayList;
import java.util.List;

//-Xms5m -Xmx5m -XX:+PrintGC
public class HeapTest03 {
    public static void main(String[] args) {
        List<byte[]> list=new ArrayList<>();
        for(int i=0;i<1000;i++){
            byte[] data=new byte[1024*1024];
            list.add(data); //假如存储到list集合，集合中还会有引用指向字节数组对象
        }
    }
}
