package com.java.jvm.oom;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 *  JVM 参数配置
 *  1)最大堆-Xmx100m
 *  2)最小堆-Xms100m
 */
public class ManyObjectOOMTests {
    public static void main(String[] args)throws Exception {
        List<byte[]> list=new ArrayList<>();
        while(true) {
            byte[] array = new byte[1024 * 1024];
            list.add(array);
            TimeUnit.MILLISECONDS.sleep(500);
        }
    }
}
