package com.java.jvm.oom;

import java.io.PrintStream;//Bootstrap ClassLoader

public class ClassObjectTests {
    public static void main(String[] args) throws ClassNotFoundException {
        //类加载时会创建字节码对象
        Class<Object> c1 = Object.class;//findClass->defineClass
        Class<?> c2=Class.forName("java.lang.Object");
        Class<?> c3=new Object().getClass();
        Class<?> c4 = Thread.currentThread().getContextClassLoader().loadClass("java.lang.Object");

        System.out.println(c1==c2);
        System.out.println(c2==c3);
        PrintStream out = System.out;
        out.println(c3==c4);

    }
}
