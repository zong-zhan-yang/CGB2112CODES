package com.java.jvm.oom;
class Outer{
    class Inner{
        int a=10;
        public void execute(){
            new Thread(()->{
                while(true){
                    System.out.println(Inner.this.a);
                    try{Thread.sleep(1000);}
                    catch (Exception e){}
                }
            }).start();
        }

        @Override
        protected void finalize() throws Throwable {
            System.out.println("inner.finalize()");
        }

    }
    @Override
    protected void finalize() throws Throwable {
        System.out.println("finalize()");
    }
}
public class OOMTest01 {
    public static void main(String[] args) {
         Outer o1=new Outer();
         Outer.Inner n1=o1.new Inner();
         n1.execute();
         o1=null;
         n1=null;
         System.gc();//手动启动GC(什么样的对象为垃圾对象，没有任何引用指向的对象)
    }
}
