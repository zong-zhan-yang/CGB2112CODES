package com.java.jvm.oom;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import java.lang.reflect.Method;
class Metaspace {
    public String greet() {
        return "Thanks for you";
    }
}
//-XX:MetaspaceSize=10M -XX:MaxMetaspaceSize=10M -XX:+PrintGC -XX:+TraceClassLoading
//-XX:MetaspaceSize=10M 元空间初始值(达到这个值时要触发fullgc)
//-XX:MaxMetaspaceSize=10M 元空间最大值

public class MetaspaceTest01 {
    public static void main(String[] args) {
        while (true) {
            Enhancer enhancer = new Enhancer();
            enhancer.setSuperclass(Metaspace.class);
            enhancer.setUseCache(false);
            enhancer.setCallback(new MethodInterceptor() {
                @Override
                public Object intercept(Object o,
                                        Method method, Object[] os, MethodProxy proxy)
                        throws Throwable {
                    System.out.println("I am proxy");
                    return proxy.invokeSuper(o,os);
                }
            });
            Metaspace proxy = (Metaspace) enhancer.create();
            proxy.greet();
        }
    }
}
