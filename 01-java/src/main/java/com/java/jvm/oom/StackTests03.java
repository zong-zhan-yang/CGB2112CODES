package com.java.jvm.oom;

public class StackTests03 {
    static void doMethod2(){
          // throw new RuntimeException("===");
        StackTraceElement[] stackTrace =
                Thread.currentThread().getStackTrace();
        for(StackTraceElement e: stackTrace){
            System.out.println(e);
        }
    }
    static void doMethod1(){
        doMethod2();
    }
    public static void main(String[] args) {
        doMethod1();
    }
}
