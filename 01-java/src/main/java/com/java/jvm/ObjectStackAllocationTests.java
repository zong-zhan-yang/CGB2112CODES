package com.java.jvm;

import java.util.concurrent.TimeUnit;

/**
 * 栈上分配测试
 * -Xmx128m -Xms128m -XX:-DoEscapeAnalysis -XX:+PrintGCDetails
 */
public class ObjectStackAllocationTests {
    public static void main(String[] args) throws InterruptedException {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            alloc();
        }
        long end = System.currentTimeMillis();
        System.out.println("花费的时间为： " + (end - start) + " ms");
        // 为了方便查看堆内存中对象个数，线程sleep
        TimeUnit.MINUTES.sleep(5);
    }
    private static void alloc() {
        byte[] data = new byte[10];//未发生逃逸
    }
}
