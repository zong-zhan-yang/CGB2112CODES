package com.java.jvm;

/**
 * -Xms2048m
 * -Xmx2048m
 */
public class JvmMemoryStructureTests {
    public static void main(String[] args) throws InterruptedException {
        //获取Java虚拟机中堆的初始内存总量(电脑物理内存大小/64)
        long initialMemory=Runtime.getRuntime().totalMemory()/1024/1024;
        //Java虚拟机试图使用的最大内存量(电脑物理内存大小/4)
        long maxMemory=Runtime.getRuntime().maxMemory()/1024/1024;
        System.out.println(String.format("-Xms%sm", initialMemory));
        System.out.println(String.format("-Xmx%sm", maxMemory));
        while(true){
            Thread.sleep(2000);
        }

    }
}
