package com.java.jvm.loader;

import java.util.HashMap;
import java.util.Map;

//-XX:+TraceClassLoading
class ClassB{//同学们思考一下，这个程序的运行结果是什么？
	private static final ClassB instance=new ClassB();
	static {
		System.out.println("ClassB");
	}
	public ClassB(){
		System.out.println(instance);
	}
}

class ClassC{//这个代码在类加载时，假如进行类的初始化，会出现空指针异常
	public static ClassC instance=new ClassC();
	public static Map<String,Object> map=new HashMap<>();
	public ClassC(){
		map.put("name", "zhangkang");
		System.out.println(map);
	}
}

public class ClassLoadingInitTests {
	public static void main(String[] args)throws Exception {
		ClassLoader loader=ClassLoader.getSystemClassLoader();
		//Class.forName("com.java.jvm.loader.ClassB", false,loader);
		Class.forName("com.java.jvm.loader.ClassC",
				true,loader);
	}
}
/**
 * ClassB加载程序运行结果分析
 * 1)Class.forName中initialize的值为false时，什么也不输出
 * 2)Class.forName中initialize的值为true时,会输出null,ClassB
 */
