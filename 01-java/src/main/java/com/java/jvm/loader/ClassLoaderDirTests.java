package com.java.jvm.loader;
import sun.misc.Launcher;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
public class ClassLoaderDirTests {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        //获取Bootstrap ClassLoader可以加载的类
        URL[] urls = Launcher.getBootstrapClassPath().getURLs();
        for(URL url:urls){
            System.out.println(url);
        }
        //获取String类的类加载器
        ClassLoader classLoader = String.class.getClassLoader();
        System.out.println(classLoader);//null (BootstrapClassLoader)

        //获取ExtClassLoader可以加载的路径
        ClassLoader parent = ClassLoader.getSystemClassLoader().getParent();
        Class<? extends ClassLoader> aClass = parent.getClass();//ExtClassLoader
        Method getExtDirs = aClass.getDeclaredMethod("getExtDirs");
        getExtDirs.setAccessible(true);
        File[] files = (File[])getExtDirs.invoke(parent);
        for(File f:files){
            System.out.println(f.getPath());
        }
    }
}
