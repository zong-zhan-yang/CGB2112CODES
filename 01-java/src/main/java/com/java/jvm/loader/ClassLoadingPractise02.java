package com.java.jvm.loader;
class C{
    static{
        System.out.println("1");
    }
    public C(){
        System.out.println("2");
    }
}
class D extends C{
    static{
        System.out.println("a");
    }
    public D(){
        System.out.println("b");
    }
}
public class ClassLoadingPractise02 {
    public static void main(String[] args) {
        C c1=new D();//先加载父类，再加载子类，所以先执行父类static{}，再执行子类static{}，构建对象子类构造方法会默认调用父类无参构造函数
        C c2=new D();//类已加载，再次构建对象不加载类
    }
    //最后的输出结果是什么？
    //1
    //a
    //2
    //b
    //2
    //b
}
