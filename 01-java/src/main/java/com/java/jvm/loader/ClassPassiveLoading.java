package com.java.jvm.loader;
class ClassAB{
    public static int a=10;
    static {
        System.out.println("AB.a="+a);
    }
}
class ClassCD extends ClassAB{
    public static int c=20;
    static {//当类是被动加载时，静态代码块是不会执行的。
        System.out.println("CD");
    }
}
public class ClassPassiveLoading {
    public static void main(String[] args) {
        //System.out.println(ClassAB.a);//ClassAB会被主动加载
        System.out.println(ClassCD.a);//这个位置ClassAB为主动加载(a变量术语ClassAB)，
        // ClassCD为被动加载(不指定初始化动作)，ClassCD中的静态代码块不会执行
    }
}
//问题：类加载时静态代码块一定会执行？错误
