package com.java.jvm.loader;

public class ObjectClassLoadingTests {
    public static void main(String[] args) throws ClassNotFoundException {
        Class<?> aClass = ClassLoader.getSystemClassLoader().
                loadClass("java.lang.Object");
        ClassLoader classLoader = aClass.getClassLoader();
        System.out.println(classLoader);
    }
}
