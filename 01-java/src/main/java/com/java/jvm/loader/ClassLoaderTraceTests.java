package com.java.jvm.loader;

class ClassA{ //JVM本身也是采用一种懒加载方式，加载我们的类。
    static int num=10;//类加载的准备阶段num的值为0，初始化阶段num的值为10
    //类加载时不一定会执行静态代码块,静态代码块是在类的初始化阶段执行的。
    static {
        System.out.println("static{}.ClassA");
    }
}
//-XX:+TraceClassLoading
public class ClassLoaderTraceTests {
    public static void main(String[] args) throws ClassNotFoundException {
        //获取类的加载器对象(AppClassLoader)
        ClassLoader systemClassLoader =
                ClassLoader.getSystemClassLoader();
        //显示加载我们自己写的ClassA
        //systemClassLoader.loadClass("com.java.jvm.loader.ClassA");

        Class.forName("com.java.jvm.loader.ClassA");

//      Class.forName("com.java.jvm.loader.ClassA",
//                true, //是否要要执行类的初始化
//                systemClassLoader);
    }
}
//面试：查看过类的字节码？怎么查看的？
//方案1：Hex-Editor插件 (此方式可以查看16字节码文件对应的16进制形式)，可以NotePad++
//方案2：直接使用jdk自带工具: javap -v ClassLoaderTraceTests.class > trace.txt
//方案3：idea中安装jclasslib插件
//面试：字节码文件由哪及部分构成(魔数，版本，常量池，访问标记，。。。。)
