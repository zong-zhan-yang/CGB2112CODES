package com.java.jvm.loader;
//JVM类加载跟踪参数
// -XX:+TraceClassLoading
public class ClassLoaderTests {//extends Object
    public static void main(String[] args) {
        System.out.println("hello class loader");
        Class<ClassLoaderTests> aClass1 = ClassLoaderTests.class;
        Class<? extends ClassLoaderTests> aClass2 =
                new ClassLoaderTests().getClass();
        System.out.println(aClass1==aClass2);
    }
}
