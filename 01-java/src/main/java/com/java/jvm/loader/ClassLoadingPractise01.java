package com.java.jvm.loader;

public class ClassLoadingPractise01 {
    static int a=10;
    static{
        a=11;
        b=11;
    }
    static int b=10;
    public static void main(String[] args) {
        System.out.println(a);
        System.out.println(b);
    }
}
