package com.java.jvm;

public class JavaThreadStackTests {
    private static int count = 1;
    public static void main(String[] args) {
        System.out.println(count);
        count++;
        main(args);//入栈操作(将方法信息存储到一个栈帧对象，然后将栈帧存储到方法栈)
    }//无限递归的结果是StackOverFlowError,但何时出现这个错误由栈的深度(大小)来决定
}
