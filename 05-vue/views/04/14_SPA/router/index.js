/*创建我们自己的路由器对象:2大步*/
//1. 先定义一个路由字典数组，保存所有相对路径与对应的页面组件之间的对应关系。
//  路由们
var routes=[
    //    相对路径         页面组件对象名
    {path:"/", component:Index},
    {path:"/details", component:Details},
    {path:"*", component:NotFound}
  ];
  //2. 再创建一个路由器对象，引入路由字典数组，形成一个整体
  var router=new VueRouter({ routes });
  //        创建一个路由器对象
  //                     引入字典数组   
  