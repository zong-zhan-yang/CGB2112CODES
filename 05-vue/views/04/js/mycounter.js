var myCounter={
    //1. 做界面
    //1.1 ***唯一父元素***
    //1.2 查找可能发生变化的位置
    //本例中: span的内容随程序变化——{{n}}
    //1.3 触发事件的元素
    //本例中: 两个按钮都能点
    //点+, +1
    //点-, -1
    template:`<div>
        <button @click="minus">-</button>
        <span>{{n}}</span>
        <button @click="add">+</button>
      </div>`,
    //2. 创建模型对象
    //2.1 创建data
    data(){
        return { //相当于以前的data
            n:0
        }
    },
    //2.2 创建methods
    methods:{
        add(){this.n++},
        minus(){if(this.n>0) this.n--}
    }
}